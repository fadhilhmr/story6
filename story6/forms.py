from . import models
from django import forms
import datetime

class EntryForm(forms.ModelForm):
    input_status = forms.CharField(widget=forms.Textarea(attrs={
        'rows':3, 
        'cols':20,
        }))
    
    class Meta:
        model = models.Status
        fields = ["input_status"]