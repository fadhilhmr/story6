from .models import Status
from .forms import EntryForm
from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.utils import timezone
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code,200)

    def test_story6_template_available(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_story6_get_model(self):
        status_obj = Status.objects.create(input_status = "halo")
        expected_obj = Status.objects.get(input_status = "halo")
        self.assertEqual(status_obj, expected_obj)

    def test_story6_validate_blank(self):
        form = EntryForm(data={'status': ''})
        self.assertFalse(form.is_valid())

    def test_story6_form_available(self):
        response = self.client.get('/')
        self.assertContains(response, '<form')
    
    def test_story6_input_are_shown(self):
        response = self.client.post('/',{"input_status" : "halo"})
        now = timezone.now
        val = self.client.get('/')
        self.assertContains(val,"halo")




class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage') #biar memorynya ga kebates
        # chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        entry = selenium.find_element_by_id('id_input_status')
        submit = selenium.find_element_by_id('inputbt')

        # Fill the form with data
        entry.send_keys('saya sedang bingung')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.assertIn('saya sedang bingung', self.selenium.page_source)

    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()


