from django.contrib import admin
from django.urls import path
from . import views

app_name='story6'
urlpatterns = [
    path('', views.input_status, name="story6"),
]
